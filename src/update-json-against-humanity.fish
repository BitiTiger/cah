#!/usr/bin/fish

echo ====== 90s black ======
python3 addCards.py ../json-against-humanity/src/90s/black.md.txt
echo ====== 90s white ======
python3 addCards.py ../json-against-humanity/src/90s/white.md.txt
git add data.db
git commit -m "updated 90s pack"

echo ====== apples black ======
python3 addCards.py ../json-against-humanity/src/apples/black.md.txt
echo ====== apples white ======
python3 addCards.py ../json-against-humanity/src/apples/white.md.txt
git add data.db
git commit -m "updated apples pack"

echo ====== Base black ======
python3 addCards.py ../json-against-humanity/src/Base/black.md.txt
echo ====== Base white ======
python3 addCards.py ../json-against-humanity/src/Base/white.md.txt
git add data.db
git commit -m "updated Base pack"

echo ====== BaseUK black ======
python3 addCards.py ../json-against-humanity/src/BaseUK/black.md.txt
echo ====== BaseUK white ======
python3 addCards.py ../json-against-humanity/src/BaseUK/white.md.txt
git add data.db
git commit -m "updated BaseUK pack"

echo ====== Box black ======
python3 addCards.py ../json-against-humanity/src/Box/black.md.txt
echo ====== Box white ======
python3 addCards.py ../json-against-humanity/src/Box/white.md.txt
git add data.db
git commit -m "updated Box pack"

echo ====== c-admin black ======
python3 addCards.py ../json-against-humanity/src/c-admin/black.md.txt
echo ====== c-admin white ======
python3 addCards.py ../json-against-humanity/src/c-admin/white.md.txt
git add data.db
git commit -m "updated c-admin pack"

echo ====== CAHe1 black ======
python3 addCards.py ../json-against-humanity/src/CAHe1/black.md.txt
echo ====== CAHe1 white ======
python3 addCards.py ../json-against-humanity/src/CAHe1/white.md.txt
git add data.db
git commit -m "updated CAHe1 pack"

echo ====== CAHe2 black ======
python3 addCards.py ../json-against-humanity/src/CAHe2/black.md.txt
echo ====== CAHe2 white ======
python3 addCards.py ../json-against-humanity/src/CAHe2/white.md.txt
git add data.db
git commit -m "updated CAHe2 pack"

echo ====== CAHe3 black ======
python3 addCards.py ../json-against-humanity/src/CAHe3/black.md.txt
echo ====== CAHe3 white ======
python3 addCards.py ../json-against-humanity/src/CAHe3/white.md.txt
git add data.db
git commit -m "updated CAHe3 pack"

echo ====== CAHe4 black ======
python3 addCards.py ../json-against-humanity/src/CAHe4/black.md.txt
echo ====== CAHe4 white ======
python3 addCards.py ../json-against-humanity/src/CAHe4/white.md.txt
git add data.db
git commit -m "updated CAHe4 pack"

echo ====== CAHe5 black ======
python3 addCards.py ../json-against-humanity/src/CAHe5/black.md.txt
echo ====== CAHe5 white ======
python3 addCards.py ../json-against-humanity/src/CAHe5/white.md.txt
git add data.db
git commit -m "updated CAHe5 pack"

echo ====== CAHe6 black ======
python3 addCards.py ../json-against-humanity/src/CAHe6/black.md.txt
echo ====== CAHe6 white ======
python3 addCards.py ../json-against-humanity/src/CAHe6/white.md.txt
git add data.db
git commit -m "updated CAHe6 pack"

echo ====== CAHgrognards black ======
python3 addCards.py ../json-against-humanity/src/CAHgrognards/black.md.txt
echo ====== CAHgrognards white ======
python3 addCards.py ../json-against-humanity/src/CAHgrognards/white.md.txt
git add data.db
git commit -m "updated CAHgrognards pack"

echo ====== Canadian black ======
python3 addCards.py ../json-against-humanity/src/Canadian/black.md.txt
echo ====== Canadian white ======
python3 addCards.py ../json-against-humanity/src/Canadian/white.md.txt
git add data.db
git commit -m "updated Canadian pack"

echo ====== c-anime black ======
python3 addCards.py ../json-against-humanity/src/c-anime/black.md.txt
echo ====== c-anime white ======
python3 addCards.py ../json-against-humanity/src/c-anime/white.md.txt
git add data.db
git commit -m "updated c-anime pack"

echo ====== c-antisocial black ======
python3 addCards.py ../json-against-humanity/src/c-antisocial/black.md.txt
echo ====== c-antisocial white ======
python3 addCards.py ../json-against-humanity/src/c-antisocial/white.md.txt
git add data.db
git commit -m "updated c-antisocial pack"

echo ====== c-derps black ======
python3 addCards.py ../json-against-humanity/src/c-derps/black.md.txt
echo ====== c-derps white ======
python3 addCards.py ../json-against-humanity/src/c-derps/white.md.txt
git add data.db
git commit -m "updated c-derps pack"

echo ====== c-doctorwho black ======
python3 addCards.py ../json-against-humanity/src/c-doctorwho/black.md.txt
echo ====== c-doctorwho white ======
python3 addCards.py ../json-against-humanity/src/c-doctorwho/white.md.txt
git add data.db
git commit -m "updated c-doctorwho pack"

echo ====== c-equinity black ======
python3 addCards.py ../json-against-humanity/src/c-equinity/black.md.txt
echo ====== c-equinity white ======
python3 addCards.py ../json-against-humanity/src/c-equinity/white.md.txt
git add data.db
git commit -m "updated c-equinity pack"

echo ====== c-eurovision black ======
python3 addCards.py ../json-against-humanity/src/c-eurovision/black.md.txt
echo ====== c-eurovision white ======
python3 addCards.py ../json-against-humanity/src/c-eurovision/white.md.txt
git add data.db
git commit -m "updated c-eurovision pack"

echo ====== c-fim black ======
python3 addCards.py ../json-against-humanity/src/c-fim/black.md.txt
echo ====== c-fim white ======
python3 addCards.py ../json-against-humanity/src/c-fim/white.md.txt
git add data.db
git commit -m "updated c-fim pack"

echo ====== c-gamegrumps black ======
python3 addCards.py ../json-against-humanity/src/c-gamegrumps/black.md.txt
echo ====== c-gamegrumps white ======
python3 addCards.py ../json-against-humanity/src/c-gamegrumps/white.md.txt
git add data.db
git commit -m "updated c-gamegrumps pack"

echo ====== c-golby black ======
python3 addCards.py ../json-against-humanity/src/c-golby/black.md.txt
echo ====== c-golby white ======
python3 addCards.py ../json-against-humanity/src/c-golby/white.md.txt
git add data.db
git commit -m "updated c-golby pack"

echo ====== c-guywglasses black ======
python3 addCards.py ../json-against-humanity/src/c-guywglasses/black.md.txt
echo ====== c-guywglasses white ======
python3 addCards.py ../json-against-humanity/src/c-guywglasses/white.md.txt
git add data.db
git commit -m "updated c-guywglasses pack"

echo ====== c-homestuck black ======
python3 addCards.py ../json-against-humanity/src/c-homestuck/black.md.txt
echo ====== c-homestuck white ======
python3 addCards.py ../json-against-humanity/src/c-homestuck/white.md.txt
git add data.db
git commit -m "updated c-homestuck pack"

echo ====== c-imgur black ======
python3 addCards.py ../json-against-humanity/src/c-imgur/black.md.txt
echo ====== c-imgur white ======
python3 addCards.py ../json-against-humanity/src/c-imgur/white.md.txt
git add data.db
git commit -m "updated c-imgur pack"

echo ====== c-khaos black ======
python3 addCards.py ../json-against-humanity/src/c-khaos/black.md.txt
echo ====== c-khaos white ======
python3 addCards.py ../json-against-humanity/src/c-khaos/white.md.txt
git add data.db
git commit -m "updated c-khaos pack"

echo ====== c-ladies black ======
python3 addCards.py ../json-against-humanity/src/c-ladies/black.md.txt
echo ====== c-ladies white ======
python3 addCards.py ../json-against-humanity/src/c-ladies/white.md.txt
git add data.db
git commit -m "updated c-ladies pack"

echo ====== c-mrman black ======
python3 addCards.py ../json-against-humanity/src/c-mrman/black.md.txt
echo ====== c-mrman white ======
python3 addCards.py ../json-against-humanity/src/c-mrman/white.md.txt
git add data.db
git commit -m "updated c-mrman pack"

echo ====== c-neindy black ======
python3 addCards.py ../json-against-humanity/src/c-neindy/black.md.txt
echo ====== c-neindy white ======
python3 addCards.py ../json-against-humanity/src/c-neindy/white.md.txt
git add data.db
git commit -m "updated c-neindy pack"

echo ====== c-nobilis black ======
python3 addCards.py ../json-against-humanity/src/c-nobilis/black.md.txt
echo ====== c-nobilis white ======
python3 addCards.py ../json-against-humanity/src/c-nobilis/white.md.txt
git add data.db
git commit -m "updated c-nobilis pack"

echo ====== c-northernlion black ======
python3 addCards.py ../json-against-humanity/src/c-northernlion/black.md.txt
echo ====== c-northernlion white ======
python3 addCards.py ../json-against-humanity/src/c-northernlion/white.md.txt
git add data.db
git commit -m "updated c-northernlion pack"

echo ====== c-prtg black ======
python3 addCards.py ../json-against-humanity/src/c-prtg/black.md.txt
echo ====== c-prtg white ======
python3 addCards.py ../json-against-humanity/src/c-prtg/white.md.txt
git add data.db
git commit -m "updated c-prtg pack"

echo ====== crabs black ======
python3 addCards.py ../json-against-humanity/src/crabs/black.md.txt
echo ====== crabs white ======
python3 addCards.py ../json-against-humanity/src/crabs/white.md.txt
git add data.db
git commit -m "updated crabs pack"

echo ====== c-ragingpsyfag black ======
python3 addCards.py ../json-against-humanity/src/c-ragingpsyfag/black.md.txt
echo ====== c-ragingpsyfag white ======
python3 addCards.py ../json-against-humanity/src/c-ragingpsyfag/white.md.txt
git add data.db
git commit -m "updated c-ragingpsyfag pack"

echo ====== c-rpanons black ======
python3 addCards.py ../json-against-humanity/src/c-rpanons/black.md.txt
echo ====== c-rpanons white ======
python3 addCards.py ../json-against-humanity/src/c-rpanons/white.md.txt
git add data.db
git commit -m "updated c-rpanons pack"

echo ====== c-rt black ======
python3 addCards.py ../json-against-humanity/src/c-rt/black.md.txt
echo ====== c-rt white ======
python3 addCards.py ../json-against-humanity/src/c-rt/white.md.txt
git add data.db
git commit -m "updated c-rt pack"

echo ====== c-socialgamer black ======
python3 addCards.py ../json-against-humanity/src/c-socialgamer/black.md.txt
echo ====== c-socialgamer white ======
python3 addCards.py ../json-against-humanity/src/c-socialgamer/white.md.txt
git add data.db
git commit -m "updated c-socialgamer pack"

echo ====== c-sodomydog black ======
python3 addCards.py ../json-against-humanity/src/c-sodomydog/black.md.txt
echo ====== c-sodomydog white ======
python3 addCards.py ../json-against-humanity/src/c-sodomydog/white.md.txt
git add data.db
git commit -m "updated c-sodomydog pack"

echo ====== c-stupid black ======
python3 addCards.py ../json-against-humanity/src/c-stupid/black.md.txt
echo ====== c-stupid white ======
python3 addCards.py ../json-against-humanity/src/c-stupid/white.md.txt
git add data.db
git commit -m "updated c-stupid pack"

echo ====== c-tg black ======
python3 addCards.py ../json-against-humanity/src/c-tg/black.md.txt
echo ====== c-tg white ======
python3 addCards.py ../json-against-humanity/src/c-tg/white.md.txt
git add data.db
git commit -m "updated c-tg pack"

echo ====== c-vewysewious black ======
python3 addCards.py ../json-against-humanity/src/c-vewysewious/black.md.txt
echo ====== c-vewysewious white ======
python3 addCards.py ../json-against-humanity/src/c-vewysewious/white.md.txt
git add data.db
git commit -m "updated c-vewysewious pack"

echo ====== c-vidya black ======
python3 addCards.py ../json-against-humanity/src/c-vidya/black.md.txt
echo ====== c-vidya white ======
python3 addCards.py ../json-against-humanity/src/c-vidya/white.md.txt
git add data.db
git commit -m "updated c-vidya pack"

echo ====== c-xkcd black ======
python3 addCards.py ../json-against-humanity/src/c-xkcd/black.md.txt
echo ====== c-xkcd white ======
python3 addCards.py ../json-against-humanity/src/c-xkcd/white.md.txt
git add data.db
git commit -m "updated c-xkcd pack"

echo ====== fantasy black ======
python3 addCards.py ../json-against-humanity/src/fantasy/black.md.txt
echo ====== fantasy white ======
python3 addCards.py ../json-against-humanity/src/fantasy/white.md.txt
git add data.db
git commit -m "updated fantasy pack"

echo ====== food black ======
python3 addCards.py ../json-against-humanity/src/food/black.md.txt
echo ====== food white ======
python3 addCards.py ../json-against-humanity/src/food/white.md.txt
git add data.db
git commit -m "updated food pack"

echo ====== GOT black ======
python3 addCards.py ../json-against-humanity/src/GOT/black.md.txt
echo ====== GOT white ======
python3 addCards.py ../json-against-humanity/src/GOT/white.md.txt
git add data.db
git commit -m "updated GOT pack"

echo ====== greenbox black ======
python3 addCards.py ../json-against-humanity/src/greenbox/black.md.txt
echo ====== greenbox white ======
python3 addCards.py ../json-against-humanity/src/greenbox/white.md.txt
git add data.db
git commit -m "updated greenbox pack"

echo ====== HACK black ======
python3 addCards.py ../json-against-humanity/src/HACK/black.md.txt
echo ====== HACK white ======
python3 addCards.py ../json-against-humanity/src/HACK/white.md.txt
git add data.db
git commit -m "updated HACK pack"

echo ====== hillary black ======
python3 addCards.py ../json-against-humanity/src/hillary/black.md.txt
echo ====== hillary white ======
python3 addCards.py ../json-against-humanity/src/hillary/white.md.txt
git add data.db
git commit -m "updated hillary pack"

echo ====== HOCAH black ======
python3 addCards.py ../json-against-humanity/src/HOCAH/black.md.txt
echo ====== HOCAH white ======
python3 addCards.py ../json-against-humanity/src/HOCAH/white.md.txt
git add data.db
git commit -m "updated HOCAH pack"

echo ====== Image1 black ======
python3 addCards.py ../json-against-humanity/src/Image1/black.md.txt
echo ====== Image1 white ======
python3 addCards.py ../json-against-humanity/src/Image1/white.md.txt
git add data.db
git commit -m "updated Image1 pack"

echo ====== matrimony black ======
python3 addCards.py ../json-against-humanity/src/matrimony/black.md.txt
echo ====== matrimony white ======
python3 addCards.py ../json-against-humanity/src/matrimony/white.md.txt
git add data.db
git commit -m "updated matrimony pack"

echo ====== misprint black ======
python3 addCards.py ../json-against-humanity/src/misprint/black.md.txt
echo ====== misprint white ======
python3 addCards.py ../json-against-humanity/src/misprint/white.md.txt
git add data.db
git commit -m "updated misprint pack"

echo ====== NSFH black ======
python3 addCards.py ../json-against-humanity/src/NSFH/black.md.txt
echo ====== NSFH white ======
python3 addCards.py ../json-against-humanity/src/NSFH/white.md.txt
git add data.db
git commit -m "updated NSFH pack"

echo ====== PAX2015 black ======
python3 addCards.py ../json-against-humanity/src/PAX2015/black.md.txt
echo ====== PAX2015 white ======
python3 addCards.py ../json-against-humanity/src/PAX2015/white.md.txt
git add data.db
git commit -m "updated PAX2015 pack"

echo ====== PAXE2013 black ======
python3 addCards.py ../json-against-humanity/src/PAXE2013/black.md.txt
echo ====== PAXE2013 white ======
python3 addCards.py ../json-against-humanity/src/PAXE2013/white.md.txt
git add data.db
git commit -m "updated PAXE2013 pack"

echo ====== PAXE2014 black ======
python3 addCards.py ../json-against-humanity/src/PAXE2014/black.md.txt
echo ====== PAXE2014 white ======
python3 addCards.py ../json-against-humanity/src/PAXE2014/white.md.txt
git add data.db
git commit -m "updated PAXE2014 pack"

echo ====== PAXEP2014 black ======
python3 addCards.py ../json-against-humanity/src/PAXEP2014/black.md.txt
echo ====== PAXEP2014 white ======
python3 addCards.py ../json-against-humanity/src/PAXEP2014/white.md.txt
git add data.db
git commit -m "updated PAXEP2014 pack"

echo ====== PAXP2013 black ======
python3 addCards.py ../json-against-humanity/src/PAXP2013/black.md.txt
echo ====== PAXP2013 white ======
python3 addCards.py ../json-against-humanity/src/PAXP2013/white.md.txt
git add data.db
git commit -m "updated PAXP2013 pack"

echo ====== PAXPP2014 black ======
python3 addCards.py ../json-against-humanity/src/PAXPP2014/black.md.txt
echo ====== PAXPP2014 white ======
python3 addCards.py ../json-against-humanity/src/PAXPP2014/white.md.txt
git add data.db
git commit -m "updated PAXPP2014 pack"

echo ====== period black ======
python3 addCards.py ../json-against-humanity/src/period/black.md.txt
echo ====== period white ======
python3 addCards.py ../json-against-humanity/src/period/white.md.txt
git add data.db
git commit -m "updated period pack"

echo ====== reject black ======
python3 addCards.py ../json-against-humanity/src/reject/black.md.txt
echo ====== reject white ======
python3 addCards.py ../json-against-humanity/src/reject/white.md.txt
git add data.db
git commit -m "updated reject pack"

echo ====== reject2 black ======
python3 addCards.py ../json-against-humanity/src/reject2/black.md.txt
echo ====== reject2 white ======
python3 addCards.py ../json-against-humanity/src/reject2/white.md.txt
git add data.db
git commit -m "updated reject2 pack"

echo ====== science black ======
python3 addCards.py ../json-against-humanity/src/science/black.md.txt
echo ====== science white ======
python3 addCards.py ../json-against-humanity/src/science/white.md.txt
git add data.db
git commit -m "updated science pack"

echo ====== trumpbag black ======
python3 addCards.py ../json-against-humanity/src/trumpbag/black.md.txt
echo ====== trumpbag white ======
python3 addCards.py ../json-against-humanity/src/trumpbag/white.md.txt
git add data.db
git commit -m "updated trumpbag pack"

echo ====== trumpvote black ======
python3 addCards.py ../json-against-humanity/src/trumpvote/black.md.txt
echo ====== trumpvote white ======
python3 addCards.py ../json-against-humanity/src/trumpvote/white.md.txt
git add data.db
git commit -m "updated trumpvote pack"

echo ====== weed black ======
python3 addCards.py ../json-against-humanity/src/weed/black.md.txt
echo ====== weed white ======
python3 addCards.py ../json-against-humanity/src/weed/white.md.txt
git add data.db
git commit -m "updated weed pack"

echo ====== www black ======
python3 addCards.py ../json-against-humanity/src/www/black.md.txt
echo ====== www white ======
python3 addCards.py ../json-against-humanity/src/www/white.md.txt
git add data.db
git commit -m "updated www pack"

echo ====== xmas2012 black ======
python3 addCards.py ../json-against-humanity/src/xmas2012/black.md.txt
echo ====== xmas2012 white ======
python3 addCards.py ../json-against-humanity/src/xmas2012/white.md.txt
git add data.db
git commit -m "updated xmas2012 pack"

echo ====== xmas2013 black ======
python3 addCards.py ../json-against-humanity/src/xmas2013/black.md.txt
echo ====== xmas2013 white ======
python3 addCards.py ../json-against-humanity/src/xmas2013/white.md.txt
git add data.db
git commit -m "updated xmas2013 pack"
